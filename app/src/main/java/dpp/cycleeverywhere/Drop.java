package dpp.cycleeverywhere;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;


public class Drop extends ActionBarActivity {


    Spinner VenuesSpinner,CyclesSpinner;
    ArrayAdapter cycleadapter,venueadapter;
    EditText RollNO;
    Context context;
    Button submit_button;
    FetchData fetchData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop);



        context=getApplicationContext();
        fetchData=new FetchData(getApplicationContext());
        fetchData.sync();

        PropertyChangeListener listener=new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent event) {
                listhandlers.post(updateLists);
            }
        };
        fetchData.addPropertyChangeListener(FetchData.SYNC_PROPERTY,listener);

        CyclesSpinner=(Spinner)findViewById(R.id.cycle_list);
        VenuesSpinner=(Spinner)findViewById(R.id.venuelist);
        progressBar=(ProgressBar)findViewById(R.id.progresbar);
        venueadapter=new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item);
        progressBar.setVisibility(View.VISIBLE);
        submit_button=(Button)findViewById(R.id.submit_button);
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Venue venue=(Venue)VenuesSpinner.getSelectedItem();
                Cycle cycle=(Cycle)CyclesSpinner.getSelectedItem();
                if(venue.vid==0||cycle.cid==0){
                    Toast.makeText(context, "Enter valid values", Toast.LENGTH_SHORT).show();
                    return;
                }

                String url=context.getString(R.string.URL_drop);
                new PostDrop().execute(url,
                        "cid", ((Cycle) CyclesSpinner.getSelectedItem()).cid,
                        "v2", ((Venue) VenuesSpinner.getSelectedItem()).vid
                );
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drop, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private Handler listhandlers= new Handler();
    private ProgressBar progressBar;
    private final Runnable updateLists=new Runnable() {
        @Override
        public void run() {
            progressBar.setVisibility(View.GONE);
            ArrayAdapter<Venue> adapter = new ArrayAdapter<>(Drop.this, android.R.layout.simple_spinner_dropdown_item);

            if(MainActivity.venues.size()==0)
                Toast.makeText(context, "Network error", Toast.LENGTH_SHORT).show();

            adapter.add(new Venue(0,"Choose Place"));
            for(Venue r:MainActivity.venues)
                adapter.add(r);
            VenuesSpinner.setAdapter(adapter);

            ArrayAdapter<Cycle> cycleArrayAdapter = new ArrayAdapter<>(Drop.this, android.R.layout.simple_spinner_dropdown_item);

            cycleArrayAdapter.add(new Cycle(0, "Choose Cycle"));
            for(Cycle r:MainActivity.pickedCycles)
                cycleArrayAdapter.add(r);

            CyclesSpinner.setAdapter(cycleArrayAdapter);
        }
    };


    private String result;
    private Handler toastHandler=new Handler();
    private Runnable toastRunner=new Runnable() {
        @Override
        public void run() {
            Toast.makeText(context,result,Toast.LENGTH_SHORT).show();
        }
    };


    private class PostDrop extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... objects) {
            // params comes from the execute() call: params[0] is the url.
            return fetchData.DownloadHelper(objects);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String response) {
            result=response;
            toastHandler.post(toastRunner);
        }
    }
}
