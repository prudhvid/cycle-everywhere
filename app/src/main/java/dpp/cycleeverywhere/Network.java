package dpp.cycleeverywhere;

import android.content.Context;


import android.app.Application;
import android.content.Context;
import android.os.Debug;
import android.test.IsolatedContext;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import dpp.cycleeverywhere.BuildConfig;
import dpp.cycleeverywhere.R;


/**
 * Created by prudhvi on 13/10/15.
 */
public class Network {
    public static int STARTED=0,WAITING=1,FAILED=3,SUCCESS=2;
    public int State;
    public int responseCode;
    Context context;
    public Network(Context context){
        this.context=context;
    }

    public String getRequest(String mUrl) throws IOException {

        InputStream is = null;
        try {
            State=STARTED;
            URL url = new URL(mUrl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection(Proxy.NO_PROXY);

            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);

            // Starts the query
            State=WAITING;
            conn.connect();
            responseCode = conn.getResponseCode();
            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = streamToString(is);

            State=SUCCESS;
            return contentAsString;
        }
        catch (Exception e)
        {
            State=FAILED;
            return "";
        }
        finally {
            if (is != null) {
                is.close();
            }
        }

    }



    public String downloadUrlBypost(String myurl,HashMap<String, String> postDataParams,boolean withProxy) throws IOException {

        InputStream is = null;
        HttpURLConnection conn=null;
        try {
            State=STARTED;
            URL url = new URL(myurl);

            Proxy proxy=new Proxy(Proxy.Type.HTTP,new InetSocketAddress(context.getString(R.string.ProxyAddress),8080));
            if(!withProxy)
                conn = (HttpURLConnection) url.openConnection();
            else
                conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(5000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            if(withProxy)
                conn.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");

            conn.setDoInput(true);
            conn.setDoOutput(true);

            State=WAITING;
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();


            responseCode = conn.getResponseCode();

            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = streamToString(is);
            State=SUCCESS;
            return contentAsString;
        }

        catch (Exception e)
        {
            State=FAILED;
            responseCode=404;
            return "";
        }
        finally {
            if (is != null) {
                is.close();
//                responseCode=conn.getResponseCode();
            }
        }

    }



    public static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
    public  static String md5(String string) {
        byte[] hash;

        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);

        for (byte b : hash) {
            int i = (b & 0xFF);
            if (i < 0x10) hex.append('0');
            hex.append(Integer.toHexString(i));
        }

        return hex.toString();
    }

    public static String streamToString(InputStream stream) throws IOException, UnsupportedEncodingException {
        BufferedReader r = new BufferedReader(new InputStreamReader(stream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return  total.toString();
    }


}