package dpp.cycleeverywhere;

/**
 * Created by Prudhvi on 10/19/2015.
 */
public class Cycle {
    public int cid;
    public String name;
    public Cycle(int cid,String name)
    {
        this.cid=cid;
        this.name=name;
    }
    public Cycle(){

    }

    @Override
    public String toString() {
        return name+"["+cid+"]";
    }
}
