package dpp.cycleeverywhere;

/**
 * Created by Prudhvi on 10/19/2015.
 */
public class Venue {
    public int vid;
    public String name;
    public Venue(int vid,String name)
    {
        this.vid=vid;
        this.name=name;
    }
    public Venue(){

    }

    @Override
    public String toString() {
        return name+"["+vid+"]";
    }
}
