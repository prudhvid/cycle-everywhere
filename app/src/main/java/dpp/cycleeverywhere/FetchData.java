package dpp.cycleeverywhere;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;


/**
 * Created by prudhvi on 19/10/15.
 */

class CycleDeserializer implements JsonDeserializer<Cycle>
{
    Context context;
    public CycleDeserializer(Context context)
    {
        this.context=context;
    }
    @Override
    public Cycle deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        com.google.gson.JsonObject jobj=json.getAsJsonObject();

        int cid=Integer.parseInt(jobj.get("cid").getAsString());
        String name=jobj.get("name").getAsString();
        try {
            Cycle cycle=new Cycle(cid,name);
            return cycle;
        }
        catch (Exception e )
        {
            Log.i("TAG", "Something is wrong with Json Deseralizer" + e.getMessage());
            return null;
        }
    }
}

class CycleCreator implements InstanceCreator {

    @Override
    public Object createInstance(Type type) {
        return new Cycle();
    }
}

class VenueDeserializer implements JsonDeserializer<Venue>
{
    Context context;
    public VenueDeserializer(Context context)
    {
        this.context=context;
    }
    @Override
    public Venue deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        com.google.gson.JsonObject jobj=json.getAsJsonObject();

        int vid=Integer.parseInt(jobj.get("vid").getAsString());
        String name=jobj.get("name").getAsString();
        try {
            Venue venue=new Venue(vid,name);
            return venue;
        }
        catch (Exception e )
        {
            Log.i("TAG", "Something is wrong with Json Deseralizer" + e.getMessage());
            return null;
        }
    }
}

class VenueCreator implements InstanceCreator {

    @Override
    public Object createInstance(Type type) {
        return new Venue();
    }
}


public class FetchData {


    public static final String SYNC_PROPERTY = "Sync_property";
    Context context;
    Network network;

    public FetchData(Context context)
    {
        this.context=context;
        network=new Network(context);
    }

    public void sync()
    {
        getCycles();
        getVenue();
        getPickedCycles();
    }


    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }


    private void getCycles()
    {
        new GetCyclesTask().execute(context.getString(R.string.URL_FreeCycles));
    }

    private void getVenue()
    {
        new GetVenueTask().execute(context.getString(R.string.URL_venues));
    }

    private void getPickedCycles() {
        new GetPickedCyclesTask().execute(context.getString(R.string.URL_picked_cycles));
    }



    private class GetCyclesTask extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... objects) {
            // params comes from the execute() call: params[0] is the url.
            return DownloadHelper(objects);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            String response=result;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Cycle.class, new CycleDeserializer(context));
            gsonBuilder.registerTypeAdapter(Cycle.class,new CycleCreator());
            gsonBuilder.serializeNulls();
            Gson gson=gsonBuilder.create();
            java.lang.reflect.Type type = new com.google.gson.reflect.TypeToken<List<Cycle>>() {}.getType();

            MainActivity.availableCycles=gson.fromJson(response,type);
            propertyChangeSupport.firePropertyChange(SYNC_PROPERTY,null,null);
        }
    }

    private class GetPickedCyclesTask extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... objects) {
            // params comes from the execute() call: params[0] is the url.
            return DownloadHelper(objects);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            String response=result;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Cycle.class, new CycleDeserializer(context));
            gsonBuilder.registerTypeAdapter(Cycle.class,new CycleCreator());
            gsonBuilder.serializeNulls();
            Gson gson=gsonBuilder.create();
            java.lang.reflect.Type type = new com.google.gson.reflect.TypeToken<List<Cycle>>() {}.getType();

            MainActivity.pickedCycles=gson.fromJson(response,type);
            propertyChangeSupport.firePropertyChange(SYNC_PROPERTY,null,null);
        }
    }




    private class GetVenueTask extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... objects) {
            // params comes from the execute() call: params[0] is the url.
            return DownloadHelper(objects);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            String response=result;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Venue.class, new VenueDeserializer(context));
            gsonBuilder.registerTypeAdapter(Venue.class,new VenueCreator());
            gsonBuilder.serializeNulls();
            Gson gson=gsonBuilder.create();
            java.lang.reflect.Type type = new com.google.gson.reflect.TypeToken<List<Venue>>() {}.getType();

            MainActivity.venues=gson.fromJson(response,type);
            propertyChangeSupport.firePropertyChange(SYNC_PROPERTY,null,null);
        }
    }



    public String DownloadHelper(Object... objects)
    {
        try {

            ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {

                String url=(String)objects[0];
                boolean get=true;
                if(objects.length>1)
                    get=false;
                String result;
                if(get)
                    result=network.getRequest(url);
                else
                {
                    HashMap<String, String> postParams = new HashMap<String, String>();
                    for (int i=1;i<objects.length;i+=2){
                        postParams.put((String)objects[i],String.valueOf(objects[i+1]));
                    }
                    result=network.downloadUrlBypost(url,postParams,false);

                }
                if(BuildConfig.DEBUG)
                    System.out.println(result);
                return result;
            }
            else {
                return "[]";
            }
        } catch (IOException e) {
            return "[]";
        }
    }
}